﻿namespace DataLogger
{
    partial class frmNewMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewMain));
            this.serialPortADAM = new System.IO.Ports.SerialPort(this.components);
            this.bgwMonthlyReport = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerMain = new System.ComponentModel.BackgroundWorker();
            this.panel20 = new System.Windows.Forms.Panel();
            this.lblMainMenuTitle = new System.Windows.Forms.Label();
            this.pnSoftwareInfo = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.lblSurfaceWaterQuality = new System.Windows.Forms.Label();
            this.lblAutomaticMonitoring = new System.Windows.Forms.Label();
            this.lblThaiNguyenStation = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblWaterLevel = new System.Windows.Forms.Label();
            this.lblHeaderNationName = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.RichTextBox();
            this.picSamplerTank = new System.Windows.Forms.PictureBox();
            this.btnLanguage = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.listen = new DataLogger.FlatButton();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtvar15Value = new System.Windows.Forms.TextBox();
            this.txtvar14 = new System.Windows.Forms.Label();
            this.txtvar15Unit = new System.Windows.Forms.Label();
            this.txtvar15 = new System.Windows.Forms.Label();
            this.txtvar14Value = new System.Windows.Forms.TextBox();
            this.txtvar16 = new System.Windows.Forms.Label();
            this.txtvar16Value = new System.Windows.Forms.TextBox();
            this.txtvar17 = new System.Windows.Forms.Label();
            this.txtvar14Unit = new System.Windows.Forms.Label();
            this.txtvar18Value = new System.Windows.Forms.TextBox();
            this.txtvar18Unit = new System.Windows.Forms.Label();
            this.txtvar16Unit = new System.Windows.Forms.Label();
            this.txtvar17Value = new System.Windows.Forms.TextBox();
            this.txtvar18 = new System.Windows.Forms.Label();
            this.txtvar17Unit = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.txtvar3 = new System.Windows.Forms.Label();
            this.txtvar3Value = new System.Windows.Forms.TextBox();
            this.txtvar3Unit = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtvar4 = new System.Windows.Forms.Label();
            this.txtvar4Value = new System.Windows.Forms.TextBox();
            this.txtvar4Unit = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtvar2 = new System.Windows.Forms.Label();
            this.txtvar2Value = new System.Windows.Forms.TextBox();
            this.txtvar2Unit = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.txtvar1 = new System.Windows.Forms.Label();
            this.txtvar1Value = new System.Windows.Forms.TextBox();
            this.txtvar1Unit = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnMPSHistoryData = new System.Windows.Forms.Button();
            this.btnMPS1Hour = new System.Windows.Forms.Button();
            this.btnMPS5Minute = new System.Windows.Forms.Button();
            this.picMPSStatus = new System.Windows.Forms.PictureBox();
            this.pnLeftSide = new System.Windows.Forms.Panel();
            this.vprgMonthlyReport = new VerticalProgressBar.VerticalProgressBar();
            this.btnMaintenance = new System.Windows.Forms.Button();
            this.btnMonthlyReport = new System.Windows.Forms.Button();
            this.btnSetting = new System.Windows.Forms.Button();
            this.btnUsers = new System.Windows.Forms.Button();
            this.btnAllHistory = new System.Windows.Forms.Button();
            this.pnHeader = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.btnLoginLogout = new System.Windows.Forms.Button();
            this.lblLoginDisplayName = new System.Windows.Forms.Label();
            this.lblHeadingTime = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel20.SuspendLayout();
            this.pnSoftwareInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSamplerTank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel30.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMPSStatus)).BeginInit();
            this.pnLeftSide.SuspendLayout();
            this.pnHeader.SuspendLayout();
            this.panel18.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // serialPortADAM
            // 
            this.serialPortADAM.PortName = "COM100";
            this.serialPortADAM.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPortADAM_DataReceived);
            // 
            // bgwMonthlyReport
            // 
            this.bgwMonthlyReport.WorkerReportsProgress = true;
            this.bgwMonthlyReport.WorkerSupportsCancellation = true;
            this.bgwMonthlyReport.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerMonthlyReport_DoWork_2);
            this.bgwMonthlyReport.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerMonthlyReport_ProgressChanged);
            this.bgwMonthlyReport.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerMonthlyReport_RunWorkerCompleted);
            // 
            // backgroundWorkerMain
            // 
            this.backgroundWorkerMain.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerMain_DoWork);
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.lblMainMenuTitle);
            this.panel20.Controls.Add(this.pnSoftwareInfo);
            this.panel20.Controls.Add(this.pictureBox52);
            this.panel20.Controls.Add(this.btnExit);
            this.panel20.Controls.Add(this.lblWaterLevel);
            this.panel20.Controls.Add(this.lblHeaderNationName);
            this.panel20.Controls.Add(this.txtData);
            this.panel20.Controls.Add(this.picSamplerTank);
            this.panel20.Controls.Add(this.btnLanguage);
            this.panel20.Controls.Add(this.pictureBox5);
            this.panel20.Controls.Add(this.listen);
            this.panel20.Controls.Add(this.button5);
            this.panel20.Controls.Add(this.button4);
            this.panel20.Location = new System.Drawing.Point(127, 426);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(28, 15);
            this.panel20.TabIndex = 70;
            this.panel20.Paint += new System.Windows.Forms.PaintEventHandler(this.panel20_Paint);
            // 
            // lblMainMenuTitle
            // 
            this.lblMainMenuTitle.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMainMenuTitle.ForeColor = System.Drawing.Color.White;
            this.lblMainMenuTitle.Location = new System.Drawing.Point(-51, 33);
            this.lblMainMenuTitle.Name = "lblMainMenuTitle";
            this.lblMainMenuTitle.Size = new System.Drawing.Size(150, 22);
            this.lblMainMenuTitle.TabIndex = 3;
            this.lblMainMenuTitle.Text = "MAIN MENU";
            this.lblMainMenuTitle.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblMainMenuTitle.Visible = false;
            this.lblMainMenuTitle.Click += new System.EventHandler(this.lblMainMenuTitle_Click);
            // 
            // pnSoftwareInfo
            // 
            this.pnSoftwareInfo.BackColor = System.Drawing.Color.White;
            this.pnSoftwareInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnSoftwareInfo.Controls.Add(this.label6);
            this.pnSoftwareInfo.Controls.Add(this.lblSurfaceWaterQuality);
            this.pnSoftwareInfo.Controls.Add(this.lblAutomaticMonitoring);
            this.pnSoftwareInfo.Controls.Add(this.lblThaiNguyenStation);
            this.pnSoftwareInfo.Controls.Add(this.pictureBox3);
            this.pnSoftwareInfo.Controls.Add(this.pictureBox2);
            this.pnSoftwareInfo.Location = new System.Drawing.Point(-416, 23);
            this.pnSoftwareInfo.Name = "pnSoftwareInfo";
            this.pnSoftwareInfo.Size = new System.Drawing.Size(34, 13);
            this.pnSoftwareInfo.TabIndex = 4;
            this.pnSoftwareInfo.Visible = false;
            this.pnSoftwareInfo.Paint += new System.Windows.Forms.PaintEventHandler(this.pnSoftwareInfo_Paint);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(145, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 14);
            this.label6.TabIndex = 6;
            this.label6.Text = "2015";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // lblSurfaceWaterQuality
            // 
            this.lblSurfaceWaterQuality.AutoSize = true;
            this.lblSurfaceWaterQuality.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurfaceWaterQuality.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblSurfaceWaterQuality.Location = new System.Drawing.Point(43, 117);
            this.lblSurfaceWaterQuality.Name = "lblSurfaceWaterQuality";
            this.lblSurfaceWaterQuality.Size = new System.Drawing.Size(214, 19);
            this.lblSurfaceWaterQuality.TabIndex = 5;
            this.lblSurfaceWaterQuality.Text = "SURFACE WATER QUALITY";
            this.lblSurfaceWaterQuality.Click += new System.EventHandler(this.lblSurfaceWaterQuality_Click);
            // 
            // lblAutomaticMonitoring
            // 
            this.lblAutomaticMonitoring.AutoSize = true;
            this.lblAutomaticMonitoring.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutomaticMonitoring.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblAutomaticMonitoring.Location = new System.Drawing.Point(43, 97);
            this.lblAutomaticMonitoring.Name = "lblAutomaticMonitoring";
            this.lblAutomaticMonitoring.Size = new System.Drawing.Size(220, 19);
            this.lblAutomaticMonitoring.TabIndex = 4;
            this.lblAutomaticMonitoring.Text = "AUTOMATIC MONITORING";
            this.lblAutomaticMonitoring.Click += new System.EventHandler(this.lblAutomaticMonitoring_Click);
            // 
            // lblThaiNguyenStation
            // 
            this.lblThaiNguyenStation.AutoSize = true;
            this.lblThaiNguyenStation.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThaiNguyenStation.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblThaiNguyenStation.Location = new System.Drawing.Point(53, 66);
            this.lblThaiNguyenStation.Name = "lblThaiNguyenStation";
            this.lblThaiNguyenStation.Size = new System.Drawing.Size(126, 19);
            this.lblThaiNguyenStation.TabIndex = 3;
            this.lblThaiNguyenStation.Text = "DMM STATION";
            this.lblThaiNguyenStation.Click += new System.EventHandler(this.lblThaiNguyenStation_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = global::DataLogger.Properties.Resources.Flag_of_South_Korea_48x32;
            this.pictureBox3.Location = new System.Drawing.Point(202, 20);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(48, 32);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::DataLogger.Properties.Resources.Flag_of_Vietnam_43x32;
            this.pictureBox2.Location = new System.Drawing.Point(76, 20);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(43, 32);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox52
            // 
            this.pictureBox52.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox52.BackgroundImage = global::DataLogger.Properties.Resources.SamplerTank_Ruler;
            this.pictureBox52.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox52.Location = new System.Drawing.Point(138, 32);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(10, 25);
            this.pictureBox52.TabIndex = 63;
            this.pictureBox52.TabStop = false;
            this.pictureBox52.Visible = false;
            this.pictureBox52.Click += new System.EventHandler(this.pictureBox52_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnExit.BackgroundImage = global::DataLogger.Properties.Resources.Shutdown_Box_Red;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.Enabled = false;
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Location = new System.Drawing.Point(154, 32);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(40, 10);
            this.btnExit.TabIndex = 7;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Visible = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblWaterLevel
            // 
            this.lblWaterLevel.AutoSize = true;
            this.lblWaterLevel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWaterLevel.Location = new System.Drawing.Point(-47, 36);
            this.lblWaterLevel.Name = "lblWaterLevel";
            this.lblWaterLevel.Size = new System.Drawing.Size(67, 15);
            this.lblWaterLevel.TabIndex = 31;
            this.lblWaterLevel.Text = "Water level";
            this.lblWaterLevel.Visible = false;
            this.lblWaterLevel.Click += new System.EventHandler(this.lblWaterLevel_Click);
            // 
            // lblHeaderNationName
            // 
            this.lblHeaderNationName.AutoSize = true;
            this.lblHeaderNationName.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeaderNationName.ForeColor = System.Drawing.Color.White;
            this.lblHeaderNationName.Location = new System.Drawing.Point(-316, 34);
            this.lblHeaderNationName.Name = "lblHeaderNationName";
            this.lblHeaderNationName.Size = new System.Drawing.Size(84, 17);
            this.lblHeaderNationName.TabIndex = 1;
            this.lblHeaderNationName.Text = "Vietnamese";
            this.lblHeaderNationName.Visible = false;
            this.lblHeaderNationName.Click += new System.EventHandler(this.lblHeaderNationName_Click);
            // 
            // txtData
            // 
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.ForeColor = System.Drawing.Color.Maroon;
            this.txtData.Location = new System.Drawing.Point(94, 41);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(48, 10);
            this.txtData.TabIndex = 62;
            this.txtData.Text = "";
            this.txtData.Visible = false;
            this.txtData.TextChanged += new System.EventHandler(this.txtData_TextChanged);
            // 
            // picSamplerTank
            // 
            this.picSamplerTank.BackColor = System.Drawing.Color.White;
            this.picSamplerTank.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picSamplerTank.Image = global::DataLogger.Properties.Resources.SamplerTankerWater;
            this.picSamplerTank.Location = new System.Drawing.Point(110, 31);
            this.picSamplerTank.Name = "picSamplerTank";
            this.picSamplerTank.Size = new System.Drawing.Size(12, 26);
            this.picSamplerTank.TabIndex = 31;
            this.picSamplerTank.TabStop = false;
            this.picSamplerTank.Visible = false;
            this.picSamplerTank.Click += new System.EventHandler(this.picSamplerTank_Click);
            // 
            // btnLanguage
            // 
            this.btnLanguage.BackColor = System.Drawing.Color.Transparent;
            this.btnLanguage.BackgroundImage = global::DataLogger.Properties.Resources.Flag_of_Vietnam_43x32;
            this.btnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLanguage.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnLanguage.FlatAppearance.BorderSize = 0;
            this.btnLanguage.Location = new System.Drawing.Point(-365, 29);
            this.btnLanguage.Name = "btnLanguage";
            this.btnLanguage.Size = new System.Drawing.Size(43, 16);
            this.btnLanguage.TabIndex = 50;
            this.btnLanguage.UseVisualStyleBackColor = false;
            this.btnLanguage.Visible = false;
            this.btnLanguage.Click += new System.EventHandler(this.btnLanguage_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::DataLogger.Properties.Resources.WaterLevel;
            this.pictureBox5.Location = new System.Drawing.Point(-107, 34);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(64, 21);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 32;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // listen
            // 
            this.listen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(71)))), ((int)(((byte)(117)))));
            this.listen.BoderWidthActive = 2;
            this.listen.BoderWidthNormal = 1;
            this.listen.ColorBoderActive = System.Drawing.Color.Green;
            this.listen.ColorBoderHover = System.Drawing.Color.Gray;
            this.listen.ColorBoderNormal = System.Drawing.Color.Silver;
            this.listen.IsActive = false;
            this.listen.Location = new System.Drawing.Point(-487, 26);
            this.listen.Margin = new System.Windows.Forms.Padding(4);
            this.listen.Name = "listen";
            this.listen.Size = new System.Drawing.Size(60, 10);
            this.listen.TabIndex = 68;
            this.listen.ToolTipHint = "";
            this.listen.Visible = false;
            this.listen.Click += new System.EventHandler(this.listen_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Transparent;
            this.button5.BackgroundImage = global::DataLogger.Properties.Resources.logo;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(141)))), ((int)(((byte)(196)))));
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(-226, 31);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(47, 22);
            this.button5.TabIndex = 67;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.BackgroundImage = global::DataLogger.Properties.Resources.clock;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(141)))), ((int)(((byte)(196)))));
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(-173, 26);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(37, 24);
            this.button4.TabIndex = 66;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // panel30
            // 
            this.panel30.AutoSize = true;
            this.panel30.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel30.BackColor = System.Drawing.Color.Transparent;
            this.panel30.BackgroundImage = global::DataLogger.Properties.Resources.main;
            this.panel30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel30.Controls.Add(this.panel5);
            this.panel30.Controls.Add(this.panel1);
            this.panel30.Controls.Add(this.panel20);
            this.panel30.Controls.Add(this.panel19);
            this.panel30.Controls.Add(this.panel17);
            this.panel30.Controls.Add(this.panel15);
            this.panel30.Controls.Add(this.btnMPSHistoryData);
            this.panel30.Controls.Add(this.btnMPS1Hour);
            this.panel30.Controls.Add(this.btnMPS5Minute);
            this.panel30.Controls.Add(this.picMPSStatus);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel30.Location = new System.Drawing.Point(89, 63);
            this.panel30.Margin = new System.Windows.Forms.Padding(10);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(885, 489);
            this.panel30.TabIndex = 65;
            this.panel30.Paint += new System.Windows.Forms.PaintEventHandler(this.panel30_Paint);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.txtvar15Value);
            this.panel5.Controls.Add(this.txtvar14);
            this.panel5.Controls.Add(this.txtvar15Unit);
            this.panel5.Controls.Add(this.txtvar15);
            this.panel5.Controls.Add(this.txtvar14Value);
            this.panel5.Controls.Add(this.txtvar16);
            this.panel5.Controls.Add(this.txtvar16Value);
            this.panel5.Controls.Add(this.txtvar17);
            this.panel5.Controls.Add(this.txtvar14Unit);
            this.panel5.Controls.Add(this.txtvar18Value);
            this.panel5.Controls.Add(this.txtvar18Unit);
            this.panel5.Controls.Add(this.txtvar16Unit);
            this.panel5.Controls.Add(this.txtvar17Value);
            this.panel5.Controls.Add(this.txtvar18);
            this.panel5.Controls.Add(this.txtvar17Unit);
            this.panel5.Location = new System.Drawing.Point(497, 442);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(44, 29);
            this.panel5.TabIndex = 116;
            this.panel5.Visible = false;
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // txtvar15Value
            // 
            this.txtvar15Value.BackColor = System.Drawing.Color.White;
            this.txtvar15Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar15Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar15Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar15Value.Location = new System.Drawing.Point(83, 66);
            this.txtvar15Value.Name = "txtvar15Value";
            this.txtvar15Value.ReadOnly = true;
            this.txtvar15Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar15Value.TabIndex = 101;
            this.txtvar15Value.Text = "27.65";
            this.txtvar15Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtvar15Value.TextChanged += new System.EventHandler(this.txtvar15Value_TextChanged);
            // 
            // txtvar14
            // 
            this.txtvar14.AutoSize = true;
            this.txtvar14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar14.ForeColor = System.Drawing.Color.Black;
            this.txtvar14.Location = new System.Drawing.Point(14, 31);
            this.txtvar14.Name = "txtvar14";
            this.txtvar14.Size = new System.Drawing.Size(47, 19);
            this.txtvar14.TabIndex = 94;
            this.txtvar14.Text = "ORP:";
            this.txtvar14.Click += new System.EventHandler(this.txtvar14_Click);
            // 
            // txtvar15Unit
            // 
            this.txtvar15Unit.AutoSize = true;
            this.txtvar15Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar15Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar15Unit.Location = new System.Drawing.Point(175, 64);
            this.txtvar15Unit.Name = "txtvar15Unit";
            this.txtvar15Unit.Size = new System.Drawing.Size(28, 19);
            this.txtvar15Unit.TabIndex = 109;
            this.txtvar15Unit.Text = "oC";
            this.txtvar15Unit.Click += new System.EventHandler(this.txtvar15Unit_Click);
            // 
            // txtvar15
            // 
            this.txtvar15.AutoSize = true;
            this.txtvar15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar15.ForeColor = System.Drawing.Color.Black;
            this.txtvar15.Location = new System.Drawing.Point(10, 66);
            this.txtvar15.Name = "txtvar15";
            this.txtvar15.Size = new System.Drawing.Size(51, 19);
            this.txtvar15.TabIndex = 95;
            this.txtvar15.Text = "Temp:";
            this.txtvar15.Click += new System.EventHandler(this.txtvar15_Click);
            // 
            // txtvar14Value
            // 
            this.txtvar14Value.BackColor = System.Drawing.Color.White;
            this.txtvar14Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar14Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar14Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar14Value.Location = new System.Drawing.Point(83, 31);
            this.txtvar14Value.Name = "txtvar14Value";
            this.txtvar14Value.ReadOnly = true;
            this.txtvar14Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar14Value.TabIndex = 100;
            this.txtvar14Value.Text = "426.17";
            this.txtvar14Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtvar14Value.TextChanged += new System.EventHandler(this.txtvar14Value_TextChanged);
            // 
            // txtvar16
            // 
            this.txtvar16.AutoSize = true;
            this.txtvar16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar16.ForeColor = System.Drawing.Color.Black;
            this.txtvar16.Location = new System.Drawing.Point(14, 107);
            this.txtvar16.Name = "txtvar16";
            this.txtvar16.Size = new System.Drawing.Size(38, 19);
            this.txtvar16.TabIndex = 96;
            this.txtvar16.Text = "DO:";
            this.txtvar16.Click += new System.EventHandler(this.txtvar16_Click);
            // 
            // txtvar16Value
            // 
            this.txtvar16Value.BackColor = System.Drawing.Color.White;
            this.txtvar16Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar16Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar16Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar16Value.Location = new System.Drawing.Point(73, 107);
            this.txtvar16Value.Name = "txtvar16Value";
            this.txtvar16Value.ReadOnly = true;
            this.txtvar16Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar16Value.TabIndex = 102;
            this.txtvar16Value.Text = "6.88";
            this.txtvar16Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtvar16Value.TextChanged += new System.EventHandler(this.txtvar16Value_TextChanged);
            // 
            // txtvar17
            // 
            this.txtvar17.AutoSize = true;
            this.txtvar17.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar17.ForeColor = System.Drawing.Color.Black;
            this.txtvar17.Location = new System.Drawing.Point(10, 141);
            this.txtvar17.Name = "txtvar17";
            this.txtvar17.Size = new System.Drawing.Size(42, 19);
            this.txtvar17.TabIndex = 97;
            this.txtvar17.Text = "TSS:";
            this.txtvar17.Click += new System.EventHandler(this.txtvar17_Click);
            // 
            // txtvar14Unit
            // 
            this.txtvar14Unit.AutoSize = true;
            this.txtvar14Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar14Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar14Unit.Location = new System.Drawing.Point(174, 29);
            this.txtvar14Unit.Name = "txtvar14Unit";
            this.txtvar14Unit.Size = new System.Drawing.Size(32, 19);
            this.txtvar14Unit.TabIndex = 108;
            this.txtvar14Unit.Text = "mV";
            this.txtvar14Unit.Click += new System.EventHandler(this.txtvar14Unit_Click);
            // 
            // txtvar18Value
            // 
            this.txtvar18Value.BackColor = System.Drawing.Color.White;
            this.txtvar18Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar18Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar18Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar18Value.Location = new System.Drawing.Point(76, 170);
            this.txtvar18Value.Multiline = true;
            this.txtvar18Value.Name = "txtvar18Value";
            this.txtvar18Value.ReadOnly = true;
            this.txtvar18Value.Size = new System.Drawing.Size(64, 21);
            this.txtvar18Value.TabIndex = 104;
            this.txtvar18Value.Text = "117.242";
            this.txtvar18Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtvar18Value.TextChanged += new System.EventHandler(this.txtvar18Value_TextChanged);
            // 
            // txtvar18Unit
            // 
            this.txtvar18Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar18Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar18Unit.Location = new System.Drawing.Point(163, 169);
            this.txtvar18Unit.Name = "txtvar18Unit";
            this.txtvar18Unit.Size = new System.Drawing.Size(48, 20);
            this.txtvar18Unit.TabIndex = 107;
            this.txtvar18Unit.Text = "uS/cm";
            this.txtvar18Unit.Click += new System.EventHandler(this.txtvar18Unit_Click);
            // 
            // txtvar16Unit
            // 
            this.txtvar16Unit.AutoSize = true;
            this.txtvar16Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar16Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar16Unit.Location = new System.Drawing.Point(166, 105);
            this.txtvar16Unit.Name = "txtvar16Unit";
            this.txtvar16Unit.Size = new System.Drawing.Size(43, 19);
            this.txtvar16Unit.TabIndex = 105;
            this.txtvar16Unit.Text = "mg/L";
            this.txtvar16Unit.Click += new System.EventHandler(this.txtvar16Unit_Click);
            // 
            // txtvar17Value
            // 
            this.txtvar17Value.BackColor = System.Drawing.Color.White;
            this.txtvar17Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar17Value.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar17Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar17Value.Location = new System.Drawing.Point(73, 141);
            this.txtvar17Value.Name = "txtvar17Value";
            this.txtvar17Value.ReadOnly = true;
            this.txtvar17Value.Size = new System.Drawing.Size(64, 19);
            this.txtvar17Value.TabIndex = 103;
            this.txtvar17Value.Text = "9.29";
            this.txtvar17Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtvar17Value.TextChanged += new System.EventHandler(this.txtvar17Value_TextChanged);
            // 
            // txtvar18
            // 
            this.txtvar18.AutoSize = true;
            this.txtvar18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar18.ForeColor = System.Drawing.Color.Black;
            this.txtvar18.Location = new System.Drawing.Point(17, 170);
            this.txtvar18.Name = "txtvar18";
            this.txtvar18.Size = new System.Drawing.Size(35, 19);
            this.txtvar18.TabIndex = 98;
            this.txtvar18.Text = "EC:";
            this.txtvar18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar18.Click += new System.EventHandler(this.txtvar18_Click);
            // 
            // txtvar17Unit
            // 
            this.txtvar17Unit.AutoSize = true;
            this.txtvar17Unit.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar17Unit.ForeColor = System.Drawing.Color.Black;
            this.txtvar17Unit.Location = new System.Drawing.Point(164, 139);
            this.txtvar17Unit.Name = "txtvar17Unit";
            this.txtvar17Unit.Size = new System.Drawing.Size(43, 19);
            this.txtvar17Unit.TabIndex = 106;
            this.txtvar17Unit.Text = "mg/L";
            this.txtvar17Unit.Click += new System.EventHandler(this.txtvar17Unit_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::DataLogger.Properties.Resources.Control;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(37, 16);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(821, 377);
            this.panel1.TabIndex = 112;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 185);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(805, 30);
            this.label1.TabIndex = 121;
            this.label1.Text = "Ống Khói Nghiền Xi 2";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(805, 30);
            this.label5.TabIndex = 120;
            this.label5.Text = "Ống Khói Nghiền Xi 1";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel10.Controls.Add(this.txtvar3);
            this.panel10.Controls.Add(this.txtvar3Value);
            this.panel10.Controls.Add(this.txtvar3Unit);
            this.panel10.Location = new System.Drawing.Point(69, 217);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(286, 146);
            this.panel10.TabIndex = 97;
            this.panel10.Paint += new System.Windows.Forms.PaintEventHandler(this.panel10_Paint);
            // 
            // txtvar3
            // 
            this.txtvar3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar3.ForeColor = System.Drawing.Color.Black;
            this.txtvar3.Location = new System.Drawing.Point(36, 16);
            this.txtvar3.Name = "txtvar3";
            this.txtvar3.Size = new System.Drawing.Size(200, 28);
            this.txtvar3.TabIndex = 47;
            this.txtvar3.Text = "Temp:";
            this.txtvar3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar3.Click += new System.EventHandler(this.txtvar3_Click);
            this.txtvar3.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar3_Paint);
            // 
            // txtvar3Value
            // 
            this.txtvar3Value.BackColor = System.Drawing.Color.White;
            this.txtvar3Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar3Value.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar3Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar3Value.Location = new System.Drawing.Point(40, 51);
            this.txtvar3Value.Name = "txtvar3Value";
            this.txtvar3Value.ReadOnly = true;
            this.txtvar3Value.Size = new System.Drawing.Size(199, 31);
            this.txtvar3Value.TabIndex = 53;
            this.txtvar3Value.Text = "27.65";
            this.txtvar3Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtvar3Value.TextChanged += new System.EventHandler(this.txtvar3Value_TextChanged);
            // 
            // txtvar3Unit
            // 
            this.txtvar3Unit.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar3Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar3Unit.Location = new System.Drawing.Point(35, 96);
            this.txtvar3Unit.Name = "txtvar3Unit";
            this.txtvar3Unit.Size = new System.Drawing.Size(201, 23);
            this.txtvar3Unit.TabIndex = 67;
            this.txtvar3Unit.Text = "oC";
            this.txtvar3Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar3Unit.Click += new System.EventHandler(this.txtvar3Unit_Click);
            this.txtvar3Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar3Unit_Paint);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel8.Controls.Add(this.txtvar4);
            this.panel8.Controls.Add(this.txtvar4Value);
            this.panel8.Controls.Add(this.txtvar4Unit);
            this.panel8.Location = new System.Drawing.Point(448, 218);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(286, 146);
            this.panel8.TabIndex = 95;
            this.panel8.Paint += new System.Windows.Forms.PaintEventHandler(this.panel8_Paint);
            // 
            // txtvar4
            // 
            this.txtvar4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar4.ForeColor = System.Drawing.Color.Black;
            this.txtvar4.Location = new System.Drawing.Point(48, 15);
            this.txtvar4.Name = "txtvar4";
            this.txtvar4.Size = new System.Drawing.Size(200, 28);
            this.txtvar4.TabIndex = 48;
            this.txtvar4.Text = "DO:";
            this.txtvar4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar4.Click += new System.EventHandler(this.txtvar4_Click);
            this.txtvar4.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar4_Paint);
            // 
            // txtvar4Value
            // 
            this.txtvar4Value.BackColor = System.Drawing.Color.White;
            this.txtvar4Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar4Value.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar4Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar4Value.Location = new System.Drawing.Point(43, 50);
            this.txtvar4Value.Name = "txtvar4Value";
            this.txtvar4Value.ReadOnly = true;
            this.txtvar4Value.Size = new System.Drawing.Size(199, 31);
            this.txtvar4Value.TabIndex = 54;
            this.txtvar4Value.Text = "6.88";
            this.txtvar4Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtvar4Value.TextChanged += new System.EventHandler(this.txtvar4Value_TextChanged);
            // 
            // txtvar4Unit
            // 
            this.txtvar4Unit.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar4Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar4Unit.Location = new System.Drawing.Point(41, 95);
            this.txtvar4Unit.Name = "txtvar4Unit";
            this.txtvar4Unit.Size = new System.Drawing.Size(201, 23);
            this.txtvar4Unit.TabIndex = 63;
            this.txtvar4Unit.Text = "mg/L";
            this.txtvar4Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar4Unit.Click += new System.EventHandler(this.txtvar4Unit_Click);
            this.txtvar4Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar4Unit_Paint);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel7.Controls.Add(this.txtvar2);
            this.panel7.Controls.Add(this.txtvar2Value);
            this.panel7.Controls.Add(this.txtvar2Unit);
            this.panel7.Location = new System.Drawing.Point(448, 39);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(286, 146);
            this.panel7.TabIndex = 94;
            this.panel7.Paint += new System.Windows.Forms.PaintEventHandler(this.panel7_Paint);
            // 
            // txtvar2
            // 
            this.txtvar2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar2.ForeColor = System.Drawing.Color.Black;
            this.txtvar2.Location = new System.Drawing.Point(48, 19);
            this.txtvar2.Name = "txtvar2";
            this.txtvar2.Size = new System.Drawing.Size(200, 28);
            this.txtvar2.TabIndex = 46;
            this.txtvar2.Text = "ORP:";
            this.txtvar2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar2.Click += new System.EventHandler(this.txtvar2_Click);
            this.txtvar2.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar2_Paint);
            // 
            // txtvar2Value
            // 
            this.txtvar2Value.BackColor = System.Drawing.Color.White;
            this.txtvar2Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar2Value.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar2Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar2Value.Location = new System.Drawing.Point(39, 53);
            this.txtvar2Value.Name = "txtvar2Value";
            this.txtvar2Value.ReadOnly = true;
            this.txtvar2Value.Size = new System.Drawing.Size(199, 31);
            this.txtvar2Value.TabIndex = 52;
            this.txtvar2Value.Text = "426.17";
            this.txtvar2Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtvar2Value.TextChanged += new System.EventHandler(this.txtvar2Value_TextChanged);
            // 
            // txtvar2Unit
            // 
            this.txtvar2Unit.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar2Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar2Unit.Location = new System.Drawing.Point(37, 98);
            this.txtvar2Unit.Name = "txtvar2Unit";
            this.txtvar2Unit.Size = new System.Drawing.Size(201, 23);
            this.txtvar2Unit.TabIndex = 66;
            this.txtvar2Unit.Text = "mV";
            this.txtvar2Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar2Unit.Click += new System.EventHandler(this.txtvar2Unit_Click);
            this.txtvar2Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar2Unit_Paint);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.BackgroundImage = global::DataLogger.Properties.Resources._0;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Controls.Add(this.txtvar1);
            this.panel6.Controls.Add(this.txtvar1Value);
            this.panel6.Controls.Add(this.txtvar1Unit);
            this.panel6.Location = new System.Drawing.Point(69, 38);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(286, 146);
            this.panel6.TabIndex = 93;
            this.panel6.Paint += new System.Windows.Forms.PaintEventHandler(this.panel6_Paint);
            // 
            // txtvar1
            // 
            this.txtvar1.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar1.ForeColor = System.Drawing.Color.Black;
            this.txtvar1.Location = new System.Drawing.Point(36, 14);
            this.txtvar1.Name = "txtvar1";
            this.txtvar1.Size = new System.Drawing.Size(200, 28);
            this.txtvar1.TabIndex = 45;
            this.txtvar1.Text = "pH:";
            this.txtvar1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar1.Click += new System.EventHandler(this.txtvar1_Click);
            this.txtvar1.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar1_Paint);
            // 
            // txtvar1Value
            // 
            this.txtvar1Value.BackColor = System.Drawing.Color.White;
            this.txtvar1Value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtvar1Value.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar1Value.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(160)))), ((int)(((byte)(186)))));
            this.txtvar1Value.Location = new System.Drawing.Point(37, 54);
            this.txtvar1Value.Name = "txtvar1Value";
            this.txtvar1Value.Size = new System.Drawing.Size(199, 31);
            this.txtvar1Value.TabIndex = 51;
            this.txtvar1Value.Text = "7.20";
            this.txtvar1Value.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtvar1Value.TextChanged += new System.EventHandler(this.txtvar1Value_TextChanged);
            // 
            // txtvar1Unit
            // 
            this.txtvar1Unit.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvar1Unit.ForeColor = System.Drawing.Color.Maroon;
            this.txtvar1Unit.Location = new System.Drawing.Point(35, 97);
            this.txtvar1Unit.Name = "txtvar1Unit";
            this.txtvar1Unit.Size = new System.Drawing.Size(201, 23);
            this.txtvar1Unit.TabIndex = 68;
            this.txtvar1Unit.Text = "aaaa";
            this.txtvar1Unit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtvar1Unit.Click += new System.EventHandler(this.txtvar1Unit_Click);
            this.txtvar1Unit.Paint += new System.Windows.Forms.PaintEventHandler(this.txtvar1Unit_Paint);
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Transparent;
            this.panel19.Location = new System.Drawing.Point(205, 3);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(40, 10);
            this.panel19.TabIndex = 74;
            this.panel19.Paint += new System.Windows.Forms.PaintEventHandler(this.panel19_Paint);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Transparent;
            this.panel17.Location = new System.Drawing.Point(207, 480);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(40, 10);
            this.panel17.TabIndex = 73;
            this.panel17.Paint += new System.Windows.Forms.PaintEventHandler(this.panel17_Paint);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Transparent;
            this.panel15.Location = new System.Drawing.Point(744, 10);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(40, 10);
            this.panel15.TabIndex = 72;
            this.panel15.Paint += new System.Windows.Forms.PaintEventHandler(this.panel15_Paint);
            // 
            // btnMPSHistoryData
            // 
            this.btnMPSHistoryData.BackColor = System.Drawing.Color.Transparent;
            this.btnMPSHistoryData.BackgroundImage = global::DataLogger.Properties.Resources._8;
            this.btnMPSHistoryData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMPSHistoryData.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnMPSHistoryData.FlatAppearance.BorderSize = 0;
            this.btnMPSHistoryData.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMPSHistoryData.Location = new System.Drawing.Point(400, 413);
            this.btnMPSHistoryData.Name = "btnMPSHistoryData";
            this.btnMPSHistoryData.Size = new System.Drawing.Size(58, 58);
            this.btnMPSHistoryData.TabIndex = 50;
            this.btnMPSHistoryData.UseVisualStyleBackColor = false;
            this.btnMPSHistoryData.Visible = false;
            this.btnMPSHistoryData.Click += new System.EventHandler(this.btnMPSHistoryData_Click);
            // 
            // btnMPS1Hour
            // 
            this.btnMPS1Hour.BackColor = System.Drawing.Color.Transparent;
            this.btnMPS1Hour.BackgroundImage = global::DataLogger.Properties.Resources._11;
            this.btnMPS1Hour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMPS1Hour.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMPS1Hour.FlatAppearance.BorderSize = 0;
            this.btnMPS1Hour.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMPS1Hour.Location = new System.Drawing.Point(284, 413);
            this.btnMPS1Hour.Name = "btnMPS1Hour";
            this.btnMPS1Hour.Size = new System.Drawing.Size(58, 58);
            this.btnMPS1Hour.TabIndex = 50;
            this.btnMPS1Hour.UseVisualStyleBackColor = false;
            this.btnMPS1Hour.Click += new System.EventHandler(this.btnMPS1Hour_Click);
            // 
            // btnMPS5Minute
            // 
            this.btnMPS5Minute.BackColor = System.Drawing.Color.Transparent;
            this.btnMPS5Minute.BackgroundImage = global::DataLogger.Properties.Resources._10;
            this.btnMPS5Minute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMPS5Minute.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMPS5Minute.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMPS5Minute.Location = new System.Drawing.Point(165, 413);
            this.btnMPS5Minute.Name = "btnMPS5Minute";
            this.btnMPS5Minute.Size = new System.Drawing.Size(58, 58);
            this.btnMPS5Minute.TabIndex = 50;
            this.btnMPS5Minute.UseVisualStyleBackColor = true;
            this.btnMPS5Minute.Click += new System.EventHandler(this.btnMPS5Minute_Click);
            // 
            // picMPSStatus
            // 
            this.picMPSStatus.BackColor = System.Drawing.Color.Transparent;
            this.picMPSStatus.BackgroundImage = global::DataLogger.Properties.Resources.Normal;
            this.picMPSStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picMPSStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picMPSStatus.Location = new System.Drawing.Point(48, 413);
            this.picMPSStatus.Name = "picMPSStatus";
            this.picMPSStatus.Size = new System.Drawing.Size(58, 58);
            this.picMPSStatus.TabIndex = 59;
            this.picMPSStatus.TabStop = false;
            this.picMPSStatus.Click += new System.EventHandler(this.picMPSStatus_Click);
            // 
            // pnLeftSide
            // 
            this.pnLeftSide.AutoSize = true;
            this.pnLeftSide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.pnLeftSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnLeftSide.Controls.Add(this.vprgMonthlyReport);
            this.pnLeftSide.Controls.Add(this.btnMaintenance);
            this.pnLeftSide.Controls.Add(this.btnMonthlyReport);
            this.pnLeftSide.Controls.Add(this.btnSetting);
            this.pnLeftSide.Controls.Add(this.btnUsers);
            this.pnLeftSide.Controls.Add(this.btnAllHistory);
            this.pnLeftSide.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnLeftSide.Location = new System.Drawing.Point(0, 53);
            this.pnLeftSide.Margin = new System.Windows.Forms.Padding(0);
            this.pnLeftSide.Name = "pnLeftSide";
            this.pnLeftSide.Size = new System.Drawing.Size(79, 509);
            this.pnLeftSide.TabIndex = 1;
            this.pnLeftSide.Paint += new System.Windows.Forms.PaintEventHandler(this.pnLeftSide_Paint);
            // 
            // vprgMonthlyReport
            // 
            this.vprgMonthlyReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.vprgMonthlyReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.vprgMonthlyReport.BorderStyle = VerticalProgressBar.BorderStyles.None;
            this.vprgMonthlyReport.Color = System.Drawing.Color.Maroon;
            this.vprgMonthlyReport.Dock = System.Windows.Forms.DockStyle.Top;
            this.vprgMonthlyReport.Location = new System.Drawing.Point(0, 70);
            this.vprgMonthlyReport.Maximum = 100;
            this.vprgMonthlyReport.Minimum = 0;
            this.vprgMonthlyReport.Name = "vprgMonthlyReport";
            this.vprgMonthlyReport.Size = new System.Drawing.Size(79, 167);
            this.vprgMonthlyReport.Step = 1;
            this.vprgMonthlyReport.Style = VerticalProgressBar.Styles.Solid;
            this.vprgMonthlyReport.TabIndex = 67;
            this.vprgMonthlyReport.Value = 90;
            this.vprgMonthlyReport.Visible = false;
            this.vprgMonthlyReport.Load += new System.EventHandler(this.vprgMonthlyReport_Load);
            // 
            // btnMaintenance
            // 
            this.btnMaintenance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnMaintenance.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMaintenance.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnMaintenance.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnMaintenance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaintenance.Image = global::DataLogger.Properties.Resources.world_clock;
            this.btnMaintenance.Location = new System.Drawing.Point(0, 253);
            this.btnMaintenance.Name = "btnMaintenance";
            this.btnMaintenance.Size = new System.Drawing.Size(79, 64);
            this.btnMaintenance.TabIndex = 50;
            this.btnMaintenance.UseVisualStyleBackColor = false;
            this.btnMaintenance.Visible = false;
            this.btnMaintenance.Click += new System.EventHandler(this.btnMaintenance_Click);
            // 
            // btnMonthlyReport
            // 
            this.btnMonthlyReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(67)))), ((int)(((byte)(96)))));
            this.btnMonthlyReport.BackgroundImage = global::DataLogger.Properties.Resources.MonthlyReportButton;
            this.btnMonthlyReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMonthlyReport.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMonthlyReport.FlatAppearance.BorderSize = 0;
            this.btnMonthlyReport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnMonthlyReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMonthlyReport.Location = new System.Drawing.Point(0, 0);
            this.btnMonthlyReport.Name = "btnMonthlyReport";
            this.btnMonthlyReport.Size = new System.Drawing.Size(79, 70);
            this.btnMonthlyReport.TabIndex = 49;
            this.btnMonthlyReport.UseVisualStyleBackColor = false;
            this.btnMonthlyReport.Click += new System.EventHandler(this.btnMonthlyReport_Click);
            // 
            // btnSetting
            // 
            this.btnSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSetting.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSetting.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetting.ForeColor = System.Drawing.SystemColors.WindowText;
            this.btnSetting.Image = global::DataLogger.Properties.Resources.applications_system_60x60;
            this.btnSetting.Location = new System.Drawing.Point(0, 317);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(79, 64);
            this.btnSetting.TabIndex = 5;
            this.btnSetting.UseVisualStyleBackColor = false;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // btnUsers
            // 
            this.btnUsers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnUsers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUsers.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnUsers.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsers.Image = global::DataLogger.Properties.Resources.user;
            this.btnUsers.Location = new System.Drawing.Point(0, 381);
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Size = new System.Drawing.Size(79, 64);
            this.btnUsers.TabIndex = 4;
            this.btnUsers.UseVisualStyleBackColor = false;
            this.btnUsers.Click += new System.EventHandler(this.btnUsers_Click);
            // 
            // btnAllHistory
            // 
            this.btnAllHistory.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnAllHistory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAllHistory.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAllHistory.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(95)))), ((int)(((byte)(133)))));
            this.btnAllHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAllHistory.Image = global::DataLogger.Properties.Resources.maintenance;
            this.btnAllHistory.Location = new System.Drawing.Point(0, 445);
            this.btnAllHistory.Name = "btnAllHistory";
            this.btnAllHistory.Size = new System.Drawing.Size(79, 64);
            this.btnAllHistory.TabIndex = 3;
            this.btnAllHistory.UseVisualStyleBackColor = false;
            this.btnAllHistory.Click += new System.EventHandler(this.btnAllHistory_Click);
            // 
            // pnHeader
            // 
            this.pnHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(56)))), ((int)(((byte)(60)))));
            this.tableLayoutPanel1.SetColumnSpan(this.pnHeader, 2);
            this.pnHeader.Controls.Add(this.label9);
            this.pnHeader.Controls.Add(this.label4);
            this.pnHeader.Controls.Add(this.label3);
            this.pnHeader.Controls.Add(this.panel18);
            this.pnHeader.Controls.Add(this.lblHeadingTime);
            this.pnHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnHeader.Location = new System.Drawing.Point(0, 0);
            this.pnHeader.Margin = new System.Windows.Forms.Padding(0);
            this.pnHeader.Name = "pnHeader";
            this.pnHeader.Size = new System.Drawing.Size(984, 53);
            this.pnHeader.TabIndex = 0;
            this.pnHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.pnHeader_Paint);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label9.Location = new System.Drawing.Point(45, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(537, 31);
            this.label9.TabIndex = 70;
            this.label9.Text = "STATION";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label4
            // 
            this.label4.Image = global::DataLogger.Properties.Resources.clock;
            this.label4.Location = new System.Drawing.Point(610, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 37);
            this.label4.TabIndex = 69;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.Image = global::DataLogger.Properties.Resources.logo;
            this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label3.Location = new System.Drawing.Point(4, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 37);
            this.label3.TabIndex = 68;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(35)))), ((int)(((byte)(39)))));
            this.panel18.Controls.Add(this.btnLoginLogout);
            this.panel18.Controls.Add(this.lblLoginDisplayName);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel18.Location = new System.Drawing.Point(806, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(178, 53);
            this.panel18.TabIndex = 65;
            this.panel18.Paint += new System.Windows.Forms.PaintEventHandler(this.panel18_Paint);
            // 
            // btnLoginLogout
            // 
            this.btnLoginLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLoginLogout.BackgroundImage = global::DataLogger.Properties.Resources.login;
            this.btnLoginLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLoginLogout.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnLoginLogout.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(141)))), ((int)(((byte)(196)))));
            this.btnLoginLogout.FlatAppearance.BorderSize = 0;
            this.btnLoginLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoginLogout.Location = new System.Drawing.Point(129, 0);
            this.btnLoginLogout.Name = "btnLoginLogout";
            this.btnLoginLogout.Size = new System.Drawing.Size(49, 53);
            this.btnLoginLogout.TabIndex = 64;
            this.btnLoginLogout.UseVisualStyleBackColor = false;
            this.btnLoginLogout.Click += new System.EventHandler(this.btnLoginLogout_Click);
            // 
            // lblLoginDisplayName
            // 
            this.lblLoginDisplayName.AutoSize = true;
            this.lblLoginDisplayName.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoginDisplayName.ForeColor = System.Drawing.Color.White;
            this.lblLoginDisplayName.Location = new System.Drawing.Point(8, 18);
            this.lblLoginDisplayName.Name = "lblLoginDisplayName";
            this.lblLoginDisplayName.Size = new System.Drawing.Size(120, 17);
            this.lblLoginDisplayName.TabIndex = 51;
            this.lblLoginDisplayName.Text = "Welcome, Guest!";
            this.lblLoginDisplayName.Click += new System.EventHandler(this.lblLoginDisplayName_Click);
            // 
            // lblHeadingTime
            // 
            this.lblHeadingTime.AutoSize = true;
            this.lblHeadingTime.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeadingTime.ForeColor = System.Drawing.Color.White;
            this.lblHeadingTime.Location = new System.Drawing.Point(653, 19);
            this.lblHeadingTime.Name = "lblHeadingTime";
            this.lblHeadingTime.Size = new System.Drawing.Size(143, 17);
            this.lblHeadingTime.TabIndex = 2;
            this.lblHeadingTime.Text = "25-09-2015 12:11:19";
            this.lblHeadingTime.Click += new System.EventHandler(this.lblHeadingTime_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.130081F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 91.86992F));
            this.tableLayoutPanel1.Controls.Add(this.pnHeader, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pnLeftSide, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel30, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(984, 562);
            this.tableLayoutPanel1.TabIndex = 69;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // frmNewMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(984, 562);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmNewMain";
            this.RightToLeftLayout = true;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Data Logger";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmNewMain_FormClosing);
            this.Load += new System.EventHandler(this.frmNewMain_Load);
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.pnSoftwareInfo.ResumeLayout(false);
            this.pnSoftwareInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSamplerTank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel30.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMPSStatus)).EndInit();
            this.pnLeftSide.ResumeLayout(false);
            this.pnHeader.ResumeLayout(false);
            this.pnHeader.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.IO.Ports.SerialPort serialPortADAM;
        public System.ComponentModel.BackgroundWorker bgwMonthlyReport;
        private System.ComponentModel.BackgroundWorker backgroundWorkerMain;
        private System.Windows.Forms.Panel panel20;
        public System.Windows.Forms.Label lblMainMenuTitle;
        public System.Windows.Forms.Panel pnSoftwareInfo;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label lblSurfaceWaterQuality;
        public System.Windows.Forms.Label lblAutomaticMonitoring;
        public System.Windows.Forms.Label lblThaiNguyenStation;
        public System.Windows.Forms.PictureBox pictureBox3;
        public System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.PictureBox pictureBox52;
        public System.Windows.Forms.Button btnExit;
        public System.Windows.Forms.Label lblWaterLevel;
        public System.Windows.Forms.Label lblHeaderNationName;
        public System.Windows.Forms.RichTextBox txtData;
        public System.Windows.Forms.PictureBox picSamplerTank;
        public System.Windows.Forms.Button btnLanguage;
        public System.Windows.Forms.PictureBox pictureBox5;
        public FlatButton listen;
        public System.Windows.Forms.Button button5;
        public System.Windows.Forms.Button button4;
        public System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel15;
        public System.Windows.Forms.Panel pnLeftSide;
        public VerticalProgressBar.VerticalProgressBar vprgMonthlyReport;
        public System.Windows.Forms.Button btnMaintenance;
        public System.Windows.Forms.Button btnMonthlyReport;
        public System.Windows.Forms.Button btnSetting;
        public System.Windows.Forms.Button btnUsers;
        public System.Windows.Forms.Button btnAllHistory;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Panel pnHeader;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel18;
        public System.Windows.Forms.Button btnLoginLogout;
        public System.Windows.Forms.Label lblLoginDisplayName;
        public System.Windows.Forms.Label lblHeadingTime;
        public System.Windows.Forms.Label txtvar15Unit;
        public System.Windows.Forms.Label txtvar14Unit;
        public System.Windows.Forms.Label txtvar18Unit;
        public System.Windows.Forms.Label txtvar17Unit;
        public System.Windows.Forms.Label txtvar16Unit;
        public System.Windows.Forms.Label txtvar17;
        public System.Windows.Forms.Label txtvar14;
        public System.Windows.Forms.Label txtvar15;
        public System.Windows.Forms.Label txtvar16;
        public System.Windows.Forms.TextBox txtvar18Value;
        public System.Windows.Forms.Label txtvar18;
        public System.Windows.Forms.TextBox txtvar17Value;
        public System.Windows.Forms.TextBox txtvar16Value;
        public System.Windows.Forms.TextBox txtvar14Value;
        public System.Windows.Forms.TextBox txtvar15Value;
        public System.Windows.Forms.Label txtvar1Unit;
        public System.Windows.Forms.Label txtvar3Unit;
        public System.Windows.Forms.Label txtvar2Unit;
        public System.Windows.Forms.Label txtvar4Unit;
        public System.Windows.Forms.Button btnMPSHistoryData;
        public System.Windows.Forms.Label txtvar1;
        public System.Windows.Forms.Button btnMPS1Hour;
        public System.Windows.Forms.Label txtvar2;
        public System.Windows.Forms.Button btnMPS5Minute;
        public System.Windows.Forms.Label txtvar3;
        public System.Windows.Forms.PictureBox picMPSStatus;
        public System.Windows.Forms.Label txtvar4;
        public System.Windows.Forms.TextBox txtvar1Value;
        public System.Windows.Forms.TextBox txtvar4Value;
        public System.Windows.Forms.TextBox txtvar2Value;
        public System.Windows.Forms.TextBox txtvar3Value;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label1;
    }
}